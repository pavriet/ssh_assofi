#ifndef __STRUCTURE_HPP__
#define __STRUCTURE_HPP__


#include "rapidjson/document.h"
#include <vector>
#include <map>
#include <fstream>
#include <string>
#include "Fichier.hpp"
#include "Dossier.hpp"


using structure_t = std::map<std::string, Dossier>;


const structure_t structureFromJson(const std::string);

#endif