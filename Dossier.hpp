#ifndef __DOSSIER_HPP__
#define __DOSSIER_HPP__

#include <iostream>
#include "Fichier.hpp"
#include <utility>
#include <string>


class Dossier {

public:
	using choix_t = std::pair<std::string, std::string>;

private:
	Fichier fichier;
	choix_t choix1;
	choix_t choix2;

public:
	static const unsigned NUM_DOSSIERS = 2;
	Dossier() {
		fichier = Fichier(); choix1 = choix_t(); choix2 = choix_t();
	}

	void setFichier(const Fichier & f) { fichier = f; }
	void setChoix1(const std::string & desc, const std::string & target) {
		choix1.first = desc;
		choix1.second = target;
	}
	void setChoix2(const std::string & desc, const std::string & target) {
		choix2.first = desc;
		choix2.second = target;
	}

	const Fichier & getFichier() const { return fichier; }
	const choix_t & getChoix1() const { return choix1; }
	const choix_t & getChoix2() const { return choix2; }
};

std::ostream & operator<<(std::ostream &, const Dossier &);

std::ostream & operator<<(std::ostream &, const Dossier::choix_t &);

#endif