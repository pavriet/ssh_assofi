#include <iostream>
#include <vector>
#include <map>

#include "Fichier.hpp"
#include "Dossier.hpp"
#include "Structure.hpp"

using vector_string_t = std::vector<std::string>;

const std::string histoire = "../histoire.json";
const std::string log_morts = "../morts.log";

constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}

void split(const std::string& str, vector_string_t& cont,
              char delim = ' ') {
    std::size_t current, previous = 0;
    current = str.find(delim);
    while (current != std::string::npos) {
        cont.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find(delim, previous);
    }
    cont.push_back(str.substr(previous, current - previous));
}




// TODO
const std::string help() {
	std::string s;
	

	return s;
}


structure_t structure = structureFromJson(histoire);

int main(int, char**) {

	/* DEBUG*/
	// for (structure_t::const_iterator it=structure.begin(); it!=structure.end(); ++it) {
	// 	std::cout << it->first << " -> " << it->second;
	// 	std::cout << "\n";
	// }

	std::string id = "0";
	Dossier courant = structure.at(id);
	std::string input;

	while (true) {
		system("clear");

		std::cout << "\n" << courant.getFichier().getTexte() << "\n";

		std::cout << "\n1 -> " << courant.getChoix1().first;
		std::cout << "\n2 -> " << courant.getChoix2().first << "\n";


		std::cout << "\n" << "Que voulez vous faire ?" << "\n";

		std::getline (std::cin, input);

		switch (str2int(input.c_str())) {
			case str2int("1"):
				std::cout << "\n" << "Choix 1" << "\n";
				id = courant.getChoix1().second;
				courant = structure.at(id);
			break;
			
			case str2int("2"):
				std::cout << "\n" << "Choix 2" << "\n";
				id = courant.getChoix2().second;
				courant = structure.at(id);
			break;

			default:
				std::cout << "\nNOPE\n";
		}

		if (id == "0") {
			std::ofstream outfile;

			outfile.open(log_morts, std::ios_base::app); // append instead of overwrite
			outfile << "mort\n";
			outfile.close();
		}

	}


	return 0;
}