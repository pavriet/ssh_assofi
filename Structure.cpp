#include "Structure.hpp"



const structure_t structureFromJson(const std::string filename) {
	structure_t structure = structure_t();

	/* Lecture de tout le fichier filename dans la variable json */
	std::ifstream t(filename);
	std::string json;
	t.seekg(0, std::ios::end);   
	json.reserve(t.tellg());
	t.seekg(0, std::ios::beg);

	json.assign((std::istreambuf_iterator<char>(t)),
				std::istreambuf_iterator<char>());

	/* parse de json */
	rapidjson::Document document = rapidjson::Document();

	document.Parse(json.c_str());

	assert(document.IsObject());

	const rapidjson::Value & root = document["root"];
	assert(root.IsArray());

	for (unsigned i = 0; i < root.Size(); i++) {
		/* parcours de la liste */
		std::string id = root[i]["id"].GetString();
		
		Dossier d = Dossier();

		const rapidjson::Value & texte = root[i]["texte"];
		assert(texte.IsArray());
		
		Fichier::string_vector sv;
		for( unsigned i=0; i<texte.GetArray().Size(); ++i) {
			sv.push_back(std::string(texte.GetArray()[i].GetString()));
		}

		
		const rapidjson::Value & choix = root[i]["choix"];
		assert(choix.IsArray());
		const rapidjson::Value & choix1 = choix.GetArray()[0];
		const rapidjson::Value & choix2 = choix.GetArray()[1];


		d.setFichier(Fichier(sv));

		if (choix1.HasMember("target")) {
			const rapidjson::Value & target1 = choix1["target"];
			d.setChoix1( choix1["desc"].GetString(), target1.GetString() );
		} else {
			d.setChoix1( choix1["desc"].GetString(), id+"-1" );
		}
		
		if (choix2.HasMember("target")) {
			const rapidjson::Value & target2 = choix2["target"];
			d.setChoix2( choix2["desc"].GetString(), target2.GetString() );
		} else {
			d.setChoix2( choix2["desc"].GetString(), id+"-2" );
		}
		

		structure[id] = d;
	}

	return structure;
}