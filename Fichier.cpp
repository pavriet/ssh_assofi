#include "Fichier.hpp"

std::ostream & operator<<(std::ostream & flux, const Fichier & f) {
	

	for (unsigned i=0; i<f.getTexte().size(); ++i) {
		flux << f.getTexte()[i] << "\n";
	}

	return flux;
}