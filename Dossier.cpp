#include "Dossier.hpp"

std::ostream & operator<<(std::ostream & flux, const Dossier & d) {
	flux << d.getFichier() << " [" << d.getChoix1() << ";" << d.getChoix2() << "]";

	return flux;
}


std::ostream & operator<<(std::ostream & flux, const Dossier::choix_t & c) {
	flux << c.first << " : " << c.second;
	return flux;
}