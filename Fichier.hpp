#ifndef __FICHIER_HPP__
#define __FICHIER_HPP__

#include <string>
#include <vector>
#include <iostream>

class Fichier {

public:
	using string_vector = std::vector<std::string>;

private:
	string_vector texte;

public:
	Fichier() : Fichier(string_vector()) {}
	Fichier(const string_vector texte) : texte(texte) {}

	const string_vector getTexte() const { return texte; }
	void setTexte(const string_vector & texte) { this->texte = texte; }
};

std::ostream & operator<<(std::ostream &, const Fichier &);


#endif